import telegram
from telegram.ext import Handler, MessageHandler, Updater
from telegram.ext.filters import Filters
import logging
from telegram import Bot, InputMediaPhoto, ParseMode
from typing import List, Dict
import os, sys, time
import urllib.request, json
from telegram.error import RetryAfter, TimedOut
import math

def log_to_telegram(token, channel_id, caption, image_urls):
    bot = Bot(token)
    if len(image_urls) == 0:
        medias = []
    else:
        medias = [
            InputMediaPhoto(
                image_urls.pop(0),
                caption = caption,
                parse_mode = ParseMode.HTML
                )
            ]
    while True:
        while len(image_urls) > 0:
            url = image_urls.pop(0)
            medias.append(InputMediaPhoto(url))
        try:
            if len(medias) == 0:
                bot.send_message(
                    chat_id = channel_id,
                    text = caption,
                    parse_mode = ParseMode.HTML
                )
            else:
                bot.send_media_group(
                    chat_id = channel_id,
                    media = medias,
                    parse_mode = ParseMode.HTML
                )
        except RetryAfter as e:
            waiting_for = int(math.ceil(e.retry_after) + 1)
            print(f"Hit flood control, waiting for {waiting_for} seconds")
            time.sleep(waiting_for)
            continue
        except TimedOut as e:
            print(f"Timed out, trying again")
            continue
        except:
            medias = []
            continue
        break

def main():
    previously_seen_ids = []
    while(True):
        print("Fetching current stock")
        available = fetch_current_items()
        print("Filtering")
        new = list(filter(lambda x: x.id not in previously_seen_ids, available))
        print("Notifying")
        notify_of_items(new)
        for item in new:
            previously_seen_ids.append(item.id)
        print("Waiting")
        time.sleep(60)

class ClearanceItem:

    def __init__(self, id: int, _type: str, price: float, flop_reason: str, size: str, firmness: str, has_cumtube: bool, has_suction_cup: bool, color: str, weight: float, images: List[str]):
        self.id = id
        self.type = _type
        self.price = price
        self.flop_reason = flop_reason
        self.size = size
        self.firmness = firmness
        self.has_cumtube = has_cumtube
        self.has_suction_cup = has_suction_cup
        self.color = color
        self.weight = weight
        self.images = images

def fetch_current_items():
    firmnesses: Dict[str, str] = {
        "2": "extra soft",
        "3": "soft",
        "4": "medium",
        "5": "medium",
        "7": "soft",
        "8": "firm",
        "9": "firm",
        "3/5": "soft shaft, med base",
        "5/3": "soft shaft, med base",
        "3/8": "soft shaft, firm base",
        "8/3": "soft shaft, firm base",
        "5/8": "med shaft, firm base",
        "8/5": "med shaft, firm base"
    }
    sizes: Dict[str, str] = {
        "1": "small",
        "2": "medium",
        "6": "one-size",
        "8": "large",
        "3": "extra large",
        "10": "mini"
    }
    with urllib.request.urlopen(os.environ["REQUEST_URL"]) as url:
        data = json.loads(url.read().decode())
        items: List[ClearanceItem] = []
        if "toys" not in data:
            return items
        for toy in data['toys']:
            image_urls: List[str] = []
            for image_object in toy['images']:
                image_urls.append(image_object['imageUrlFull'])
            clearance_item: ClearanceItem = ClearanceItem(
                toy['id'],
                toy['sku'],
                toy['price'],
                (None if len(toy['external_flop_reason']) == 0 else toy['external_flop_reason'].strip()) if ("external_flop_reason" in toy and toy["external_flop_reason"] is not None) else None,
                sizes[str(toy['size'])] if str(toy['size']) in sizes else f"unknown ({toy['size']})",
                firmnesses[str(toy['firmness'])] if str(toy['firmness']) in firmnesses else f"unknown ({toy['firmness']})",
                True if toy['cumtube'] == 13 else False,
                True if toy['suction_cup'] == 23 else False,
                (toy['color_display'] if "name" not in toy['colorTheme'] else toy['colorTheme']['name']) if "colorTheme" in toy else "Unknown",
                toy['weight'],
                image_urls
                )
            items.append(clearance_item)
        return items

def notify_of_items(items: List[ClearanceItem]):
    if "TELEGRAM_API_KEY" in os.environ and "TELEGRAM_CHANNEL_ID" in os.environ:
        token = os.environ["TELEGRAM_API_KEY"]
        channel_id = int(os.environ["TELEGRAM_CHANNEL_ID"])
    else:
        print("Skipping Telegram, no TELEGRAM_API_KEY or no TELEGRAM_CHANNEL_ID")
        token = None
        channel_id = None
    for item in items:
        notify_of_item(item, token, channel_id)

def notify_of_item(item: ClearanceItem, token: str, channel_id: int):
    lines: List[str] = []
    lines.append(f"New <b>{item.type.capitalize()}</b> in inventory")
    lines.append("<b>Clearance</b>" if item.flop_reason == None else f"<b>Flop</b> ({item.flop_reason})")
    lines.append(f"<b>{item.size.capitalize()}</b> size, <b>{item.firmness.capitalize()}</b> firmness")
    if(item.has_cumtube):
        if(item.has_suction_cup):
            lines.append("Has <b>cumtube</b> and <b>suction cup</b>")
        else:
            lines.append("Has <b>cumtube</b>, lacks <i>suction cup</i>")
    else:
        if(item.has_suction_cup):
            lines.append("Has <b>suction cup</b>, lacks <i>cumtube</i>")
        else:
            lines.append("Lacks <i>cumtube</i> and <i>suction cup</i>")
    lines.append(f"In {item.color} color, for <b>${item.price}</b>, {item.weight} kg")
    print("--------------------")
    for line in lines:
        print(line)
    if(token != None):
        log_to_telegram(token, channel_id, "\n".join(lines), item.images)
    for image in item.images:
        print(f"Image: {image}")

if(__name__ == "__main__"):
    main()
